﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Oap2DynamicsCrm.Oap;
using Oap2DynamicsCrm.Oap.Models;
using Oap2DynamicsCrm.Xrm;
using ServiceStack.Text;
using System;
using System.Configuration;
using System.Globalization;
using System.Linq;

namespace Oap2DynamicsCrm.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            var xrm = new XrmServiceContext("Xrm");

            //// Save affiliates to the system so that they can 
            ////be matched later with customers.
            //SaveAffiliates(xrm);

            // Save everyone as a lead first. This will allow
            // CRM to setup funnels.
            //SaveLeads(xrm);
            SavePurchases(xrm);

            Console.WriteLine("Press Any Key To Exit");
            Console.ReadKey();
        }

        private static void SavePurchases(XrmServiceContext xrm)
        {
            var products = xrm.ProductSet;

            // Get Affiliates
            var page = 1;

            var tInfo = new CultureInfo("en-US", false).TextInfo;

            var purchases = GetPurchases(page);
            while (purchases.Purchase.Count > 0)
            {
                foreach (var purchase in purchases.Purchase)
                {
                    // Get the customer.
                    var customer = xrm.ContactSet
                        .Where(c => c.dstech_OAPId == purchase.ContactId)
                        .SingleOrDefault();

                    // If no customer, get the lead
                    if (customer == null)
                    {
                        // Qualify the lead and return the customer.
                        customer = QualifyLead(xrm, purchase, customer);
                    }

                    // Look for an order with the OAP ID.
                    var order = xrm.SalesOrderSet
                        .Where(o => o.dstech_OAPId == purchase.Id)
                        .SingleOrDefault();

                    // If there is not an order, create one.
                    if (order == null)
                    {
                        // Get the default price list.
                        var priceLevel = xrm.PriceLevelSet
                            .Where(p => p.Name == "Retail")
                            .SingleOrDefault();

                        // Create the order
                        order = new SalesOrder
                        {
                            order_customer_contacts = customer,
                            DateFulfilled = purchase.Date,
                            price_level_orders = priceLevel,
                            BillTo_City = customer.Address1_City,
                            BillTo_StateOrProvince = customer.Address1_StateOrProvince,
                            BillTo_Country = customer.Address1_Country,
                            BillTo_PostalCode = customer.Address1_PostalCode,
                            BillTo_ContactName = customer.FullName,
                            BillTo_Line1 = customer.Address1_Line1,
                            BillTo_Line2 = customer.Address1_Line2,
                            BillTo_Telephone = customer.Address1_Telephone1,
                            //B
                        };
                    }

                    throw new Exception("test");
                    
                }

                // Get the next page of records from OAP.
                page++;
                purchases = GetPurchases(page);
            }

        }

        private static Xrm.Contact QualifyLead(XrmServiceContext xrm, Purchase purchase, Xrm.Contact customer)
        {
            // look for a lead.
            var lead = xrm.LeadSet
                .Where(l => l.dstech_OAPId == purchase.ContactId)
                .SingleOrDefault();

            // If lead null, get the customer from OAP and create a contact.
            if (lead == null)
            {
                throw new Exception("No Lead Or Contact for sale");
            }
            // else convert lead to contact. Ensure that OAP ID is set with the new contact.   
            else
            {
                // Qualify the lead to move to contact.
                QualifyLeadRequest leadRequest = new QualifyLeadRequest
                {
                    CreateAccount = false,
                    CreateOpportunity = false,
                    CreateContact = true,
                    LeadId = lead.ToEntityReference(),
                    Status = new OptionSetValue(3)
                };

                var qualifyIntoAccountContactRes =
                    (QualifyLeadResponse)xrm.Execute(leadRequest);

                // This should only loop once as we are looking at a specific lead.
                foreach (var entity in qualifyIntoAccountContactRes.CreatedEntities)
                {
                    // Fill in the customer from the new entity
                    customer = xrm.ContactSet
                        .Where(c => c.Id == entity.Id)
                        .SingleOrDefault();
                }

                customer.dstech_OAPId = lead.dstech_OAPId;
                xrm.UpdateObject(customer);
                xrm.SaveChanges();
            }
            return customer;
        }

        private static void SaveAffiliates(XrmServiceContext xrm)
        {
            Console.WriteLine("Saving Affiliates");
            // Get affiliate programs
            var affiliatePrograms = xrm.dstech_affiliateprogramSet;

            // Get Affiliates
            var page = 1;
            var affiliates = GetAffiliateResult(page);

            var tInfo = new CultureInfo("en-US", false).TextInfo;

            while (affiliates.Contact.Count > 0)
            {
                // Process Each contact as a lead.
                foreach (var affiliate in affiliates.Contact)
                {
                    // Get contact info for the affiliate
                    var affiliateInfo = affiliate.GroupTag
                        .Find(g => g.Name == "Contact Information");

                    // Get the affiliate program id for the affiliate. Used
                    // to match to system in Dynamics CRM.
                    var affiliateProgramId = GetFieldText(affiliate.GroupTag
                        .Find(g => g.Name == "Affiliate Data")
                        .Field.Find(f => f.Name == "Affiliate Program"));

                    // Set the affiliate program that the affiliate belongs to in CRM.
                    dstech_affiliateprogram affiliateProgram = 
                        SetAffiliateProgram(affiliatePrograms, affiliateProgramId);

                    // Try to see if the contact exists.
                    var contact = xrm.ContactSet
                        .Where(c =>
                            c.EMailAddress1 == affiliateInfo.Field
                                .Find(f => f.Name == "E-Mail").Text)
                        .FirstOrDefault();


                    // If the contact doesn't exist, add them.
                    if (contact == null)
                    {
                        contact = CreateContact(xrm, tInfo, affiliate, affiliateInfo, contact);
                    }

                    // Add the affiliate program to the contact if it exists.
                    if (affiliateProgram != null)
                    {
                        // Add the contact to the affiliate program.
                        contact.dstech_ContactId = affiliateProgram.ToEntityReference();
                        xrm.UpdateObject(contact);
                        xrm.SaveChanges();
                    }
                }

                // Get the next page of records from OAP.
                page++;
                affiliates = GetAffiliateResult(page);
            }
        }

        private static dstech_affiliateprogram SetAffiliateProgram(IQueryable<dstech_affiliateprogram> affiliatePrograms, string affiliateProgramId)
        {
            dstech_affiliateprogram affiliateProgram = null;

            if (!String.IsNullOrEmpty(affiliateProgramId))
            {
                // Pull the affiliate program from CRM based on id passed by OAP.
                switch (affiliateProgramId)
                {
                    case "1":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - Platinum")
                            .SingleOrDefault();

                        break;
                    case "2":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - Jefreaux")
                            .SingleOrDefault();

                        break;
                    case "3":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - Gold")
                            .SingleOrDefault();

                        break;
                    case "4":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - David Bass")
                            .SingleOrDefault();

                        break;
                    case "5":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - Kevin Puls")
                            .SingleOrDefault();

                        break;
                    case "6":
                        affiliateProgram = affiliatePrograms
                            .Where(a => a.dstech_name == "VSL Generator Affiliates - Benson Gold")
                            .SingleOrDefault();

                        break;
                }
            }
            return affiliateProgram;
        }

        // Create a new contact with data from OAP.
        private static Xrm.Contact CreateContact(XrmServiceContext xrm, TextInfo tInfo, Oap.Models.Contact oapContact, GroupTag affiliateInfo, Xrm.Contact contact)
        {
            contact = new Oap2DynamicsCrm.Xrm.Contact
            {
                FirstName = tInfo.ToTitleCase(GetFieldText(affiliateInfo.Field.Find(f => f.Name == "First Name")).ToLower()),
                LastName = tInfo.ToTitleCase(GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Last Name"))),
                EMailAddress1 = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "E-Mail")).ToLower(),
                Address1_City = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "City")).ToLower(),
                Address1_Line1 = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Address")),
                Address1_Line2 = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Address 2")),
                Address1_StateOrProvince = tInfo.ToTitleCase(GetFieldText(affiliateInfo.Field.Find(f => f.Name == "State")).ToLower()),
                Address1_PostalCode = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Zip Code")),
                Address1_Country = tInfo.ToTitleCase(GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Country")).ToLower()),
                Address1_Telephone1 = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Home Phone")),
                Address1_Telephone2 = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Office Phone")),
                MobilePhone = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Cell Phone")),
                WebSiteUrl = GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Website")),
                DoNotBulkEMail = ((oapContact.BulkMail != "1") || (oapContact.BulkMail != "2")),
                dstech_OAPId = oapContact.Id
            };


            xrm.AddObject(contact);
            xrm.SaveChanges();
            return contact;
        }

        // Return an empty string if the text portion of the field is null.
        private static string GetFieldText(Field field)
        {
            if (field.Text != null)
                return field.Text;
            else
                return String.Empty;
        }

        private static void SaveLeads(XrmServiceContext xrm)
        {
            Console.WriteLine("Saving Affiliates");
            int page = 1;

            // Search Customers
            var leads = GetSearchResult(page);

            var tInfo = new CultureInfo("en-US", false).TextInfo;

            // While there are contacts to process, do so.
            while (leads.Contact.Count > 0)
            {
                // Process Each contact as a lead.
                foreach (var oapContact in leads.Contact)
                {
                    var contactInfo = oapContact.GroupTag
                        .Find(g => g.Name == "Contact Information");

                    var affiliateInfo = oapContact.GroupTag
                        .Find(g => g.Name == "Lead Information");

                    // See if a lead exists.
                    var lead = xrm.LeadSet
                        .Where(l =>
                            l.EMailAddress1 == GetFieldText(contactInfo.Field.Find(f => f.Name == "E-Mail")))
                        .FirstOrDefault();

                    // Try to see if the contact exists.
                    var contact = xrm.ContactSet
                        .Where(c =>
                            c.EMailAddress1 == GetFieldText(contactInfo.Field
                                .Find(f => f.Name == "E-Mail")))
                        .FirstOrDefault();

                    var tester = contactInfo.Field.Find(f => f.Name == "First Name");

                    // If the OAP contact doesn't exist as a lead or contact in CRM, add them as a lead.
                    if ((lead == null) && (contact == null))
                    {
                        lead = SaveLead(xrm, tInfo, oapContact, contactInfo, affiliateInfo, lead);
                    }
                }

                // Get the next page of records.
                page++;
                leads = GetSearchResult(page);
            }
        }

        private static Lead SaveLead(XrmServiceContext xrm, TextInfo tInfo, Oap.Models.Contact oapContact, GroupTag contactInfo, GroupTag affiliateInfo, Lead lead)
        {
            // Create the lead.
            lead = new Oap2DynamicsCrm.Xrm.Lead
            {
                FirstName = tInfo.ToTitleCase(GetFieldText(contactInfo.Field.Find(f => f.Name == "First Name")).ToLower()),
                LastName = tInfo.ToTitleCase(GetFieldText(contactInfo.Field.Find(f => f.Name == "Last Name")).ToLower()),
                EMailAddress1 = GetFieldText(contactInfo.Field.Find(f => f.Name == "E-Mail")).ToLower(),
                Address1_City = GetFieldText(contactInfo.Field.Find(f => f.Name == "City")).ToLower(),
                Address1_Line1 = GetFieldText(contactInfo.Field.Find(f => f.Name == "Address")),
                Address1_Line2 = GetFieldText(contactInfo.Field.Find(f => f.Name == "Address 2")),
                Address1_StateOrProvince = tInfo.ToTitleCase(GetFieldText(contactInfo.Field.Find(f => f.Name == "State")).ToLower()),
                Address1_PostalCode = GetFieldText(contactInfo.Field.Find(f => f.Name == "Zip Code")),
                Address1_Country = tInfo.ToTitleCase(GetFieldText(contactInfo.Field.Find(f => f.Name == "Country")).ToLower()),
                Address1_Telephone1 = GetFieldText(contactInfo.Field.Find(f => f.Name == "Home Phone")),
                Address1_Telephone2 = GetFieldText(contactInfo.Field.Find(f => f.Name == "Office Phone")),
                MobilePhone = GetFieldText(contactInfo.Field.Find(f => f.Name == "Cell Phone")),
                CompanyName = GetFieldText(contactInfo.Field.Find(f => f.Name == "Fax")),
                WebSiteUrl = GetFieldText(contactInfo.Field.Find(f => f.Name == "Website")),
                DoNotBulkEMail = ((oapContact.BulkMail != "1") || (oapContact.BulkMail != "2")),
                dstech_OAPId = oapContact.Id
            };

            // Get the affiliates for the lead, if there is one.
            var firstAffiliate = xrm.ContactSet
                .Where(c => c.FullName == GetFieldText(affiliateInfo.Field.Find(f => f.Name == "First Referrer")))
                .FirstOrDefault();

            // Get the affiliates for the lead, if there is one.
            var lastAffiliate = xrm.ContactSet
                .Where(c => c.FullName == tInfo.ToTitleCase(GetFieldText(affiliateInfo.Field.Find(f => f.Name == "Last Referrer")).ToLower()))
                .FirstOrDefault();

            // Set the affiliates for this lead.
            lead.dstech_contact_lead_first_referrer = firstAffiliate;
            lead.dstech_contact_lead_last_referrer = lastAffiliate;

            // Save the lead.
            xrm.AddObject(lead);
            xrm.SaveChanges();
            return lead;
        }

        // Get contacts from OAP that have signed up as affiliates.
        private static Result GetAffiliateResult(int page)
        {
            // Setup OAP Search query
            Search search = new Search();
            search.Page = page.ToString();
            search.Equation.Add(new Equation
            {
                Field = "Affiliate Program",
                Op = "n",
                Value = ""
            });


            // Get the user info from OAP.
            Api api = new Api(ConfigurationManager.AppSettings["OapContactApiUrl"]);
            api.appid = ConfigurationManager.AppSettings["OapAppId"];
            api.key = ConfigurationManager.AppSettings["OapKey"];
            api.reqType = "search";
            api.return_id = "1";
            api.data = search.ToXmlString().UrlEncode();


            var oapReturn = api.GetDataAsync();
            oapReturn.Wait();

            var oapData = (Result)oapReturn.Result.ToResponse(typeof(Result));

            return oapData;
        }

        // Get contacts from OAP that have an email address.
        private static Result GetSearchResult(int page)
        {
            // Setup OAP Search query
            Search search = new Search();
            search.Page = page.ToString();
            search.Equation.Add(new Equation
            {
                Field = "E-Mail",
                Op = "n",
                Value = ""
            });


            // Get the user info from OAP.
            Api api = new Api(ConfigurationManager.AppSettings["OapContactApiUrl"]);
            api.appid = ConfigurationManager.AppSettings["OapAppId"];
            api.key = ConfigurationManager.AppSettings["OapKey"];
            api.reqType = "search";
            api.return_id = "1";
            api.data = search.ToXmlString().UrlEncode();


            var oapReturn = api.GetDataAsync();
            oapReturn.Wait();

            var oapData = (Result)oapReturn.Result.ToResponse(typeof(Result));

            return oapData;
        }

        // Get contacts from OAP that have signed up as affiliates.
        private static Result GetPurchases(int page)
        {
            // Setup OAP Search query
            Search search = new Search();
            search.Page = page.ToString();
            search.Equation.Add(new Equation
            {
                Field = "Price",
                Op = "g",
                Value = "-1.00"
            });


            // Get the user info from OAP.
            Api api = new Api(ConfigurationManager.AppSettings["OapProductApiUrl"]);
            api.appid = ConfigurationManager.AppSettings["OapAppId"];
            api.key = ConfigurationManager.AppSettings["OapKey"];
            api.reqType = "search_purchase";
            api.return_id = "1";
            api.data = search.ToXmlString().UrlEncode();


            var oapReturn = api.GetDataAsync();
            oapReturn.Wait();

            var oapData = (Result)oapReturn.Result.ToResponse(typeof(Result));

            return oapData;
        }

        private static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

    }
}
