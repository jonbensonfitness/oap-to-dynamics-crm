﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap.Models
{
    [XmlRoot(ElementName = "purchases")]
    public class Purchases
    {
        public List<Purchase> Purchase { get; set; }
    }

    [XmlRoot(ElementName = "purchase")]
    public class Purchase
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("product_id")]
        public string ProductId { get; set; }

        [XmlAttribute("contact_id")]
        public string ContactId { get; set; }

        [XmlElement("field")]
        public List<Field> Field { get; set; }

        [XmlIgnore()]
        public string ProductName
        {
            get
            {
                return Field.Find(f => f.Name == "Product Name").Text;
            }
        }

        [XmlIgnore()]
        public decimal Price
        {
            get
            {
                return Convert.ToDecimal(Field.Find(f => f.Name == "Price").Text);
            }
        }

        [XmlIgnore()]
        public decimal Qty
        {
            get
            {
                return Convert.ToDecimal(Field.Find(f => f.Name == "Quantity").Text);
            }
        }

        [XmlIgnore()]
        public DateTime Date
        {
            get
            {
                return ConvertFromUnixTimestamp(Convert.ToDouble(Field.Find(f => f.Name == "Date").Text));
            }
        }

        [XmlIgnore()]
        public string DateString
        {
            get
            {
                return Date.ToString("MM/dd/yyyy");
            }
        }

        [XmlIgnore()]
        public decimal Total
        {
            get
            {
                return Convert.ToDecimal(Field.Find(f => f.Name == "Total").Text);
            }
        }



        static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddSeconds(timestamp);
        }
    }
}
