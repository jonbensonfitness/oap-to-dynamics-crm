﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap.Models
{
    [XmlRoot(ElementName = "contact")]
    public class Contact
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("date")]
        public string Date { get; set; }

        [XmlAttribute("dlm")]
        public string Dlm { get; set; }

        [XmlAttribute("score")]
        public string Score { get; set; }

        [XmlAttribute("purl")]
        public string Purl { get; set; }

        [XmlAttribute("bulk_mail")]
        public string BulkMail { get; set; }

        [XmlElement("Group_Tag")]
        public List<GroupTag> GroupTag { get; set; }

        [XmlElement("tag")]
        public List<string> Tag { get; set; }
    }

    [XmlRoot("Group_Tag")]
    public class GroupTag
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("field")]
        public List<Field> Field { get; set; }
    }

    [XmlRoot("field")]
    public class Field
    {
        [XmlText]
        public string Text { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }
    }

    [XmlRoot("contact_id")]
    public class ContactDeleteRequest
    {
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot("contact_id")]
    public class ContactRequest
    {
        [XmlText]
        public string Text { get; set; }
    }
}
