﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap.Models
{
    [Serializable()]
    [XmlRoot("search")]
    public class Search
    {
        public Search()
        {
            Equation = new List<Equation>();
        }

        [XmlAttribute("page")]
        public string Page { get; set; }

        [XmlElement("equation")]
        public List<Equation> Equation { get; set; }
    }

    [Serializable()]
    [XmlRoot("equation")]
    public class Equation
    {
        [XmlElement("field")]
        public string Field { get; set; }

        [XmlElement("op")]
        public string Op { get; set; }

        [XmlElement("value")]
        public string Value { get; set; }
    }
}
