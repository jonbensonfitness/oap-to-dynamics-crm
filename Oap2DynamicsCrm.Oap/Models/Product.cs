﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap.Models
{
    [XmlRoot(ElementName = "product")]
    public class Product
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlElement("Group_Tag")]
        public List<GroupTag> GroupTag { get; set; }

        [XmlIgnore()]
        public string ProductName
        {
            get
            {
                return GroupTag.Find(g => g.Name == "Product Details").Field.Find(f => f.Name == "Name").Text;
            }
        }

        [XmlIgnore()]
        public decimal Price
        {
            get
            {
                return Convert.ToDecimal(GroupTag.Find(g => g.Name == "Product Details").Field.Find(f => f.Name == "Price").Text.Replace("$", ""));
            }
        }

        [XmlIgnore()]
        public string Description
        {
            get
            {
                return GroupTag.Find(g => g.Name == "Product Details").Field.Find(f => f.Name == "Description").Text;
            }
        }

        [XmlIgnore()]
        public string CommissionLevel1
        {
            get
            {
                return GroupTag.Find(g => g.Name == "Product Details").Field.Find(f => f.Name == "Commission (Level 1)").Text;
            }
        }

        [XmlIgnore()]
        public string CommissionLevel2
        {
            get
            {
                return GroupTag.Find(g => g.Name == "Product Details").Field.Find(f => f.Name == "Commission (Level 2)").Text;
            }
        }
    }
}
