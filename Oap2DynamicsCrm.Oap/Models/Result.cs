﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap.Models
{
    [XmlRoot("result")]
    public class Result
    {
        [XmlText]
        public string Status { get; set; }

        [XmlElement("error")]
        public string Error { get; set; }

        [XmlElement("contact")]
        public List<Contact> Contact { get; set; }

        [XmlElement("product")]
        public List<Product> Product { get; set; }

        [XmlArray("purchases")]
        [XmlArrayItem("purchase")]
        public List<Purchase> Purchase { get; set; }
    }
}
