﻿using System.Collections.Generic;
using System.Net;

namespace Oap2DynamicsCrm.Oap
{
    public class Response
    {
        public string Result { get; set; }
        public HttpStatusCode Status { get; set; }
        public Dictionary<string, string> Errors { get; set; }
    }
}
