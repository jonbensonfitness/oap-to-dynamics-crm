﻿using Oap2DynamicsCrm.Oap.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Oap2DynamicsCrm.Oap
{
    public static class XmlExtension
    {
        public static string ToXmlString(this object request)
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            XmlDocument doc = new XmlDocument();


            XmlSerializer xs = new XmlSerializer(request.GetType());
            MemoryStream ms = new MemoryStream();

            StreamReader reader = new StreamReader(ms);//Works

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlWriter writer = XmlWriter.Create(ms, settings);

            try
            {
                xs.Serialize(writer, request, ns);
                writer.Flush();
                ms.Position = 0;

                doc.PreserveWhitespace = false;
                doc.LoadXml(reader.ReadToEnd());


                foreach (XmlElement el in doc.SelectNodes("descendant::*[not(*) and not(normalize-space())]"))
                {
                    el.IsEmpty = false;
                }

                return doc.DocumentElement.OwnerDocument.OuterXml;
            }
            catch
            {
                return "";
            }
            finally
            {
                if (ms != null) ms.Close();
                if (reader != null) reader.Close();
            }
        }

        // Convert an Interspire XML response into the type expected.
        public static object ToResponse(this string response, Type type)
        {

            var byteArray = Encoding.ASCII.GetBytes(response);
            XmlSerializer xs = new XmlSerializer(type);

            Result result = null;

            using (var stream = new MemoryStream(byteArray))
            {
                result = (Result)xs.Deserialize(stream);
            }

            return result;
        }
    }
}
