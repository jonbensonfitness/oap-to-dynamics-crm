﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Oap2DynamicsCrm.Oap
{
    public class Api
    {
        public Api()
        {
        }

        public Api(string url)
        {
            Url = url;
        }

        // Data for the API
        public string appid { get; set; }
        public string key { get; set; }
        public string reqType { get; set; }
        public string data { get; set; }
        public string f_add { get; set; }
        public string return_id { get; set; }
        public string count { get; set; }

        public string Url { get; set; }

        private IEnumerable<KeyValuePair<string, string>> GetFormValues()
        {
            var formValues = new List<KeyValuePair<string, string>>();

            formValues.Add(new KeyValuePair<string, string>("appid", appid));
            formValues.Add(new KeyValuePair<string, string>("key", key));
            formValues.Add(new KeyValuePair<string, string>("reqType", reqType));

            if (!String.IsNullOrEmpty(return_id))
                formValues.Add(new KeyValuePair<string, string>("return_id", return_id));

            if (!String.IsNullOrEmpty(data))
                formValues.Add(new KeyValuePair<string, string>("data", data));

            return formValues;
        }

        public async Task<string> GetDataAsync()
        {
            Dictionary<string, string> errors = new Dictionary<string, string>();

            using (var client = new HttpClient())
            {
                var formValues = new FormUrlEncodedContent(GetFormValues());

                var result = client.PostAsync(Url, formValues).Result;
                return result.Content.ReadAsStringAsync().Result;
            }
        }
    }
}
